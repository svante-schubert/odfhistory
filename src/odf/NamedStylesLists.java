package odf;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.annotation.Nullable;

public class NamedStylesLists {
	final private Map<String, StyleNames> lists = new HashMap<String, StyleNames>();

	@Nullable
	StyleNames get(String entry) {
		if (!entry.endsWith("content.xml") && !entry.endsWith("styles.xml")) {
			return null;
		}
		StyleNames names = lists.get(entry);
		if (names == null) {
			StyleNames styles = null;
			if (entry.endsWith("content.xml")) {
				String stylesEntry = entry.substring(0, entry.length() - 11)
						+ "styles.xml";
				styles = lists.get(stylesEntry);
			}
			names = new StyleNames(styles);
			lists.put(entry, names);
		}
		return names;
	}

}
