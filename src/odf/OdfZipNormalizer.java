package odf;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;

import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class OdfZipNormalizer {

	final OdfNormalizer odfNormalizer;

	OdfZipNormalizer() {
		odfNormalizer = new OdfNormalizer();
	}

	public void normalizeFlat(File input, OutputStream output)
			throws TransformerException, XMLStreamException, IOException,
			SAXException, NormalizeException {
		FileInputStream in = new FileInputStream(input);
		OdfInfo info = odfNormalizer.odfInfo;
		if (input.getAbsolutePath().endsWith(".rng")) {
			info = odfNormalizer.rngInfo;
		}
		StyleNames styleNames = new StyleNames();
		Document doc = odfNormalizer.xml.parse(in, info, styleNames);
		in.close();
		if (NC.rng.equals(doc.getDocumentElement().getNamespaceURI())) {
			RngNormalizer.normalize(doc);
		} else {
			normalizeOdf(doc, odfNormalizer, styleNames);
		}
		OdfHistory.pretty(doc, output, info.getRngInfo(odfNormalizer.xml)
				.getElementsWithNoText());
	}

	public void normalize(Path input, Path output) throws ZipException,
			IOException, SAXException, TransformerException,
			NormalizeException, XMLStreamException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		normalize(input.toFile(), out);
		out.close();
		Files.createDirectories(output.getParent());
		Files.write(output, out.toByteArray());
	}

	public void normalize(File input, OutputStream output) throws ZipException,
			IOException, SAXException, TransformerException,
			NormalizeException, XMLStreamException {
		final ZipFile zip;
		try {
			zip = new ZipFile(input);
		} catch (IOException e) {
			normalizeFlat(input, output);
			return;
		}
		final String mimetype = getMimetype(zip);
		final Document manifest = getManifest(zip, odfNormalizer);
		LinkedList<String> entries = OdfNormalizer.cleanManifest(manifest);
		odfNormalizer.odfManifestInfo.version = getVersion(manifest);
		validate(manifest, odfNormalizer.odfManifestInfo);
		odfNormalizer.odfInfo.version = odfNormalizer.odfManifestInfo.version;

		ZipOutputStream out = new ZipOutputStream(output);
		out.setLevel(9);
		addUncompressedEntry(out, mimetype.getBytes(), "mimetype");
		writeXML(out, manifest, "META-INF/manifest.xml",
				odfNormalizer.odfManifestInfo.getRngInfo(odfNormalizer.xml)
						.getElementsWithNoText());
		NamedStylesLists styleNames = new NamedStylesLists();

		// handle all entries, make sure to handle styles.xml before the
		// corresponding content.xml so that named styles are known when
		// handling content.xml
		Set<String> handledFiles = new HashSet<String>();
		for (String entry : entries) {
			if (entry.equals("content.xml") || entry.endsWith("/content.xml")) {
				String stylesEntry = entry.substring(0, entry.length() - 11)
						+ "styles.xml";
				if (entries.contains(stylesEntry)
						&& !handledFiles.contains(stylesEntry)) {
					System.out.println(stylesEntry);
					normalizeEntry(zip, zip.getEntry(stylesEntry), out,
							odfNormalizer, styleNames.get(entry));
					handledFiles.add(stylesEntry);
				}
			}
			if (!handledFiles.contains(entry)) {
				System.out.println(entry);
				normalizeEntry(zip, zip.getEntry(entry), out, odfNormalizer,
						styleNames.get(entry));
				handledFiles.add(entry);
			}
		}

		out.close();
		zip.close();
	}

	static private void addUncompressedEntry(ZipOutputStream zip, byte b[],
			String path) throws IOException {
		ZipEntry ze = new ZipEntry(path);
		CRC32 checksum = new CRC32();
		checksum.update(b);
		ze.setMethod(ZipEntry.STORED);
		ze.setSize(b.length);
		ze.setCrc(checksum.getValue());
		zip.putNextEntry(ze);
		zip.write(b);
		zip.closeEntry();
	}

	static private void normalizeEntry(ZipFile zip, @Nullable ZipEntry entry,
			ZipOutputStream out, OdfNormalizer odfNormalizer,
			@Nullable StyleNames styleNames) throws IOException, SAXException,
			TransformerException, NormalizeException, XMLStreamException {
		if (entry == null || entry.isDirectory()) {
			return;
		}
		final String name = entry.getName();
		if (name.endsWith(".xml")) {
			if (entry.getSize() == 0) {
				copy(zip, entry, out);
			} else {
				normalizeXml(zip, entry, out, odfNormalizer, styleNames);
			}
		} else {
			copy(zip, entry, out);
		}
	}

	static private void normalizeXml(ZipFile zip, ZipEntry entry,
			ZipOutputStream out, OdfNormalizer normalizer,
			@Nullable StyleNames styleNames) throws IOException, SAXException,
			TransformerException, NormalizeException, XMLStreamException {
		Path entryPath = Paths.get(entry.getName());
		InputStream in = zip.getInputStream(entry);
		Document doc = normalizer.xml.parse(in, normalizer.odfInfo, styleNames);
		in.close();
		if (entryPath.endsWith("content.xml")
				|| entryPath.endsWith("styles.xml")
				|| entryPath.endsWith("meta.xml")) {
			normalizeOdf(doc, normalizer, styleNames);
		}
		writeXML(out, doc, entry.getName(),
				normalizer.odfInfo.getRngInfo(normalizer.xml)
						.getElementsWithNoText());
	}

	static private void normalizeOdf(Document doc, OdfNormalizer normalizer,
			@Nullable StyleNames styleNames) throws TransformerException,
			IOException, SAXException, NormalizeException, XMLStreamException {
		if (styleNames == null) {
			styleNames = new StyleNames();
		}
		if (NC.office.equals(doc.getDocumentElement().getNamespaceURI())) {
			validate(doc, normalizer.odfInfo);
			normalizer.normalize(doc, styleNames);
			validate(doc, normalizer.odfInfo);
		}
	}

	static private void copy(ZipFile zip, ZipEntry entry, ZipOutputStream out)
			throws IOException {
		InputStream in = zip.getInputStream(entry);
		final String name = entry.getName();
		ZipEntry ze = new ZipEntry(name);
		int method = entry.getMethod();
		if (name.endsWith(".svg") || name.endsWith(".ttf") || name.endsWith(".rdf")) {
			method = ZipEntry.DEFLATED;
		}
		ze.setMethod(method);
		ze.setSize(entry.getSize());
		ze.setTime(entry.getTime());
		ze.setCrc(entry.getCrc());
		out.putNextEntry(ze);

		byte[] buffer = new byte[128 * 1024];
		int len;
		while ((len = in.read(buffer)) != -1) {
			out.write(buffer, 0, len);
		}
		out.closeEntry();
		in.close();
	}

	static private String getMimetype(ZipFile zip) throws IOException {
		ZipEntry entry = zip.getEntry("mimetype");
		byte buffer[] = new byte[(int) entry.getSize()];
		InputStream in = zip.getInputStream(entry);
		if (in.read(buffer) != buffer.length) {
			throw new IOException("Did not read entire manifest.");
		}
		in.close();
		return new String(buffer, "utf-8");
	}

	static private Document getManifest(ZipFile zip, OdfNormalizer normalizer)
			throws IOException, SAXException, TransformerException {
		InputStream in = zip.getInputStream(zip
				.getEntry("META-INF/manifest.xml"));
		Document doc = normalizer.xml.parse(in, normalizer.odfManifestInfo,
				null);
		in.close();
		return doc;
	}

	static private void writeXML(ZipOutputStream out, Document doc,
			String path, HashSet<QName> elementsWithNoText) throws IOException,
			TransformerException, XMLStreamException {
		ZipEntry ze = new ZipEntry(path);
		ze.setMethod(ZipEntry.DEFLATED);
		out.putNextEntry(ze);
		OdfHistory.pretty(doc, out, elementsWithNoText);
		out.closeEntry();
	}

	static private String getVersion(Document doc) {
		Element e = doc.getDocumentElement();
		String version = e.getAttributeNS(e.getNamespaceURI(), "version");
		return ("".equals(version) || version == null) ? "1.1" : version;
	}

	static private void validate(Document doc, OdfInfo info)
			throws TransformerException, IOException, SAXException,
			XMLStreamException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		OdfHistory.pretty(doc, out, new HashSet<QName>());
		out.close();
		ByteArrayInputStream b = new ByteArrayInputStream(out.toByteArray());
		info.errorHandler.reset();
		if ("1.2".equals(info.version)) {
			info.getDriver12().validate(new InputSource(b));
		} else {
			info.getDriver11().validate(new InputSource(b));
		}
		if (info.errorHandler.errors.size() > 0) {
			System.err.println(info.errorHandler.errors);
			throw new SAXException(info.errorHandler.errors.toString());
		}
	}
}
