package odf;

import java.util.Stack;

import org.eclipse.jdt.annotation.Nullable;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.XMLFilterImpl;

// fix odf validation errors that occur in the ODF spec
class OdfFixer extends XMLFilterImpl {

	final String version;
	@Nullable
	final private StyleNames styleNames;

	private int muteLevel = -1;

	private boolean automaticStyles;

	private int level = 0;

	final private Stack<String> familyStack = new Stack<String>();

	OdfFixer(String version, @Nullable StyleNames styleNames) {
		this.version = version;
		this.styleNames = styleNames;
	}

	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		familyStack.clear();
		muteLevel = -1;
		automaticStyles = false;
	}

	@Override
	public void startElement(@Nullable String uri, @Nullable String localName,
			@Nullable String qName, @Nullable Attributes attributes)
			throws SAXException {
		if (uri == null || localName == null || qName == null
				|| attributes == null) {
			throw new NullPointerException();
		}

		level += 1;
		automaticStyles = automaticStyles
				|| (NC.office.equals(uri) && "automatic-styles"
						.equals(localName));

		// correct invalid attributes
		AttributesImpl atts = new AttributesImpl(attributes);
		int l = atts.getLength();
		String family = null;
		String styleName = null;
		for (int i = 0; i < l; ++i) {
			String u = atts.getURI(i);
			String ln = atts.getLocalName(i);
			String v = atts.getValue(i);
			if (NC.style.equals(u)) {
				if ("vertical-align".equals(ln)) {
					if (!("automatic".equals(v) || "bottom".equals(v)
							|| "middle".equals(v) || "top".equals(v))) {
						atts.removeAttribute(i--);
					}
				} else if ("keep-together".equals(ln)) {
					atts.removeAttribute(i--);
				} else if ("layout-grid-snap-to-characters".equals(ln)) {
					atts.removeAttribute(i--);
				} else if ("text-position".equals(ln) && "".equals(v)) {
					atts.removeAttribute(i--);
				} else if ("family".equals(ln)) {
					family = v;
				} else if ("name".equals(ln)) {
					styleName = v;
				}
			} else if (NC.fo.equals(u)) {
				if ("hyphenation-push-char-count".equals(ln)) {
					atts.setValue(i, fixPositiveInteger(v));
				} else if ("hyphenation-remain-char-count".equals(ln)) {
					atts.setValue(i, fixPositiveInteger(v));
				} else if ("clip".equals(ln)) {
					if ("rect(0in 0in 0in 0in)".equals(v)) {
						atts.setValue(i, "rect(0in, 0in, 0in, 0in)");
					}
				}
			} else if (NC.text.equals(u)) {
				if ("start-value".equals(ln)) {
					atts.setValue(i, fixPositiveInteger(v));
				} else if ("style-name".equals(ln)) {
					if ("list-level-style-image".equals(localName)
							&& NC.text.equals(uri)) {
						atts.removeAttribute(i--);
						System.out.println("Removing " + ln + " " + uri + " "
								+ localName);
					}
				}
			} else if ("".equals(u)) {
				// This attribute might be written by LibreOffice in
				// <number:number/>
				if ("min-decimal-places".equals(ln) && NC.number.equals(uri)
						&& "number".equals(localName)) {
					atts.removeAttribute(i--);
				}
			}
		}
		if (NC.text.equals(uri)) {
			if ("outline-style".equals(localName)) {
				// this seems wrong
				int i = atts.getIndex(NC.style, "name");
				if ("1.2".equals(version)) {
					if (i == -1) {
						atts.addAttribute(NC.style, "name", "style:name",
								"string", "OOPS");
					}
				} else if (i != -1) {
					atts.removeAttribute(i);
				}
			} else if ("changed-region".equals(localName)) {
				int i = atts.getIndex(NC.xml, "id"), j = atts.getIndex(NC.text,
						"id");
				if ("1.2".equals(version) && i == -1 && j != -1) {
					atts.addAttribute(NC.xml, "id", "xml:id", "string",
							atts.getValue(j));
				}
			}
		} else if (NC.form.equals(uri)) {
			if ("form".equals(localName)) {
				int i = atts.getIndex(NC.xlink, "type");
				if (i == -1) {
					atts.addAttribute(NC.xlink, "type", "xlink:type", "string",
							"simple");
				}
			}
		}
		// correct invalid nesting
		// element <style:style style:family="paragraph"> may not have child
		// element <style:graphic-properties/> or <graphic-properties/>
		// (workaround for invalid ODF from LibreOffice 5
		if ("graphic-properties".equals(localName)
				&& (NC.style.equals(uri) || "".equals(uri))
				&& familyStack.size() > 0
				&& "paragraph".equals(familyStack.lastElement())) {
			skipElement();
		}
		if (family != null && styleName != null && styleNames != null) {
			styleNames.addName(styleName, family, automaticStyles);
		}
		familyStack.push(family);
		if (muteLevel == -1) {
			super.startElement(uri, localName, qName, atts);
		}
	}

	private void skipElement() {
		if (muteLevel == -1) {
			muteLevel = level;
		}
	}

	private static String fixPositiveInteger(String v) {
		String nv = v;
		try {
			int val = Integer.parseInt(v);
			if (val < 1) {
				nv = "1";
			}
		} catch (NumberFormatException e) {
			nv = "1";
		}
		return nv;
	}

	@Override
	public void endElement(@Nullable String uri, @Nullable String localName,
			@Nullable String qName) throws SAXException {
		if (muteLevel == -1) {
			super.endElement(uri, localName, qName);
		}
		familyStack.pop();
		level -= 1;
		if (muteLevel > level) {
			muteLevel = -1;
		}
	}

	@Override
	public void characters(@Nullable char[] ch, int start, int length)
			throws SAXException {
		if (muteLevel == -1) {
			super.characters(ch, start, length);
		}
	}

	@Override
	public void ignorableWhitespace(@Nullable char[] ch, int start, int length)
			throws SAXException {
	}

}
