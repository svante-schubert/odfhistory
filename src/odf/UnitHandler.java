package odf;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jdt.annotation.Nullable;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.XMLFilterImpl;

public class UnitHandler extends XMLFilterImpl {

	final private Map<String, HashMap<String, Fixer>> fixers = new HashMap<String, HashMap<String, Fixer>>();
	final private HashMap<String, HashMap<String, HashMap<String, HashMap<String, Fixer>>>> fixers2 = new HashMap<String, HashMap<String, HashMap<String, HashMap<String, Fixer>>>>();

	final private Fixer nonNegativeLengthFixer = new NonNegativeLengthFixer();

	final private Fixer borderFixer = new BorderFixer();

	UnitHandler(OdfRngInfo info) {
		// todo: point3D and clipShape
		// length
		addPattern(
				info,
				"-?([0-9]+(\\.[0-9]*)?|\\.[0-9]+)((cm)|(mm)|(in)|(pt)|(pc)|(px))",
				nonNegativeLengthFixer);
		// nonNegativeLength
		addPattern(
				info,
				"([0-9]+(\\.[0-9]*)?|\\.[0-9]+)((cm)|(mm)|(in)|(pt)|(pc)|(px))",
				nonNegativeLengthFixer);
		// positiveLength
		addPattern(
				info,
				"([0-9]*[1-9][0-9]*(\\.[0-9]*)?|0+\\.[0-9]*[1-9][0-9]*|\\.[0-9]*[1-9][0-9]*)((cm)|(mm)|(in)|(pt)|(pc)|(px))",
				nonNegativeLengthFixer);
		add(NC.fo, "border-bottom", borderFixer);
		add(NC.fo, "border-left", borderFixer);
		add(NC.fo, "border-right", borderFixer);
		add(NC.fo, "border-top", borderFixer);
		add(NC.fo, "border", borderFixer);
	}

	private void addPattern(OdfRngInfo info, String pattern, Fixer fixer) {
		HashSet<QName> atts = info.getStringAttributesWithPattern(pattern);
		for (QName qname : atts) {
			add(qname.namespaceURI, qname.localName, fixer);
		}
	}

	private void add(String uri, String localName, Fixer fixer) {
		HashMap<String, Fixer> m = fixers.get(localName);
		if (m == null) {
			m = new HashMap<String, Fixer>();
			fixers.put(localName, m);
		}
		m.put(uri, fixer);
	}

	private void add(String euri, String elocalName, String auri,
			String alocalName, Fixer fixer) {
		HashMap<String, HashMap<String, HashMap<String, Fixer>>> m = fixers2
				.get(alocalName);
		if (m == null) {
			m = new HashMap<String, HashMap<String, HashMap<String, Fixer>>>();
			fixers2.put(alocalName, m);
		}
		HashMap<String, HashMap<String, Fixer>> m2 = m.get(elocalName);
		if (m2 == null) {
			m2 = new HashMap<String, HashMap<String, Fixer>>();
			m.put(elocalName, m2);
		}
		HashMap<String, Fixer> m3 = m2.get(auri);
		if (m3 == null) {
			m3 = new HashMap<String, Fixer>();
			m2.put(auri, m3);
		}
		m3.put(euri, fixer);
	}

	@Nullable
	Fixer getFixer(String euri, String elocalName, String auri,
			String alocalName) {
		Fixer f = null;
		HashMap<String, Fixer> m = fixers.get(alocalName);
		if (m != null) {
			f = m.get(auri);
		} else {
			HashMap<String, HashMap<String, HashMap<String, Fixer>>> m1 = fixers2
					.get(alocalName);
			if (m1 != null) {
				HashMap<String, HashMap<String, Fixer>> m2 = m1.get(elocalName);
				if (m2 != null) {
					HashMap<String, Fixer> m3 = m2.get(auri);
					if (m3 != null) {
						f = m3.get(euri);
					}
				}
			}
		}
		return f;
	}

	@Override
	public void startElement(@Nullable String uri, @Nullable String localName,
			@Nullable String qName, @Nullable Attributes attributes)
			throws SAXException {
		if (uri == null || localName == null || qName == null
				|| attributes == null) {
			throw new NullPointerException();
		}

		AttributesImpl atts = new AttributesImpl(attributes);
		int l = atts.getLength();
		for (int i = 0; i < l; ++i) {
			Fixer fixer = getFixer(uri, localName, atts.getURI(i),
					atts.getLocalName(i));
			if (fixer != null) {
				atts.setValue(i, fixer.fix(atts.getValue(i)));
			}
		}
		super.startElement(uri, localName, qName, atts);
	}
}

interface Fixer {
	String fix(String value);
}

class LosslessNumberConversion {

	final Matcher matcher = Pattern.compile(
			"^(-?[0-9]*\\.?([0-9]*[1-9])?)0*(in|cm|mm|pc|pt|px)$").matcher("");

	static final DecimalFormat format = new DecimalFormat();

	static {
		format.setMinimumFractionDigits(0);
	}

	/**
	 * Convert the given length to either cm or pt units.
	 *
	 * If the value can be losslessly converted to cm, this is done. Otherwise,
	 * the value is converted losslessly to pt.
	 *
	 * @param string
	 * @return
	 */
	@Nullable
	String convert(String string) {
		matcher.reset(string);
		if (!matcher.find()) {
			return null;
		}
		final float value = Float.parseFloat(matcher.group(1));
		final String unit = matcher.group(3);
		final String fraction = matcher.group(2);
		final int fractionDigits = (fraction == null) ? 0 : fraction.length();
		if (unit.equals("cm")) {
			format.setMaximumFractionDigits(fractionDigits);
			return format.format(value) + "cm";
		}
		if (unit.equals("in")) {
			format.setMaximumFractionDigits(fractionDigits + 2);
			return format.format(2.54 * value) + "cm";
		}
		if (unit.equals("mm")) {
			format.setMaximumFractionDigits(fractionDigits + 1);
			return format.format(value / 10) + "cm";
		}
		final int integer = (int) Math.round(Math.pow(10, fractionDigits)
				* value);
		if (unit.equals("pc")) {
			if (integer % 6 == 0) {
				format.setMaximumFractionDigits(fractionDigits + 2);
				return format.format(value * 127 / 600) + "cm";
			}
			format.setMaximumFractionDigits(fractionDigits);
			return format.format(12 * value) + "pt";
		}
		if (unit.equals("pt")) {
			if (integer % 36 == 0) {
				format.setMaximumFractionDigits(fractionDigits + 2);
				return format.format(value * 127 / 3600) + "cm";
			}
			format.setMaximumFractionDigits(fractionDigits);
			return format.format(value) + "pt";
		}
		// unit is px
		if (integer % 48 == 0) {
			format.setMaximumFractionDigits(fractionDigits + 2);
			return format.format(value * 127 / 4800) + "cm";
		}
		format.setMaximumFractionDigits(fractionDigits + 2);
		return format.format(value * 3 / 4) + "pt";
	}

	@Nullable
	String convertPt(String string, int fractionDigits) {
		matcher.reset(string);
		if (!matcher.find()) {
			return null;
		}
		double value = Float.parseFloat(matcher.group(1));
		final String unit = matcher.group(3);
		switch (unit) {
		case "cm":
			value = value * 72 / 2.54;
			break;
		case "mm":
			value = value * 72 / 25.4;
			break;
		case "in":
			value = value * 72;
			break;
		case "pc":
			value = value * 12;
			break;
		case "px":
			value = value * 3 / 4;
			break;
		}
		// hardcoded limit on precision! can be lossy!
		format.setMaximumFractionDigits(fractionDigits);
		return format.format(value) + "pt";

	}
}

class NonNegativeLengthFixer implements Fixer {

	final private LosslessNumberConversion nc = new LosslessNumberConversion();

	public String fix(String value) {
		String v = nc.convert(value);
		return (v == null) ? value : v;
	}
}

class BorderFixer implements Fixer {

	final private LosslessNumberConversion nc = new LosslessNumberConversion();

	public String fix(String value) {
		int index = value.indexOf(' ');
		if (index != -1) {
			String number = value.substring(0, index);
			number = nc.convertPt(number, 2);
			if (number != null) {
				value = number + value.substring(index);
			}
		}
		return value;
	}
}