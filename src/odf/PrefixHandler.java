package odf;

import org.eclipse.jdt.annotation.Nullable;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.XMLFilterImpl;

// replace prefixes with preferred prefixes
class PrefixHandler extends XMLFilterImpl {
	final static NC nc = new NC();

	@Override
	public void startPrefixMapping(@Nullable String prefix, @Nullable String uri)
			throws SAXException {
		String p = nc.getPrefix(uri);
		super.startPrefixMapping((p == null) ? prefix : p, uri);
	}

	private String getQName(String uri, String localName, String qName) {
		String prefix = nc.getPrefix(uri);
		return (prefix == null) ? qName : prefix + ":" + localName;
	}

	@Override
	public void startElement(@Nullable String uri, @Nullable String localName,
			@Nullable String qName, @Nullable Attributes attributes)
			throws SAXException {
		if (uri == null || localName == null || qName == null) {
			throw new NullPointerException("Unexpected null value.");
		}
		qName = getQName(uri, localName, qName);

		AttributesImpl atts = new AttributesImpl(attributes);
		int l = atts.getLength();
		for (int i = 0; i < l; ++i) {
			String qname = atts.getQName(i);
			if ("xmlns".equals(qname) || qname.startsWith("xmlns:")) {
				String prefix = nc.getPrefix(uri);
				if (prefix != null) {
					atts.setQName(i, "xmlns:" + prefix);
				}
			} else {
				atts.setQName(i,
						getQName(atts.getURI(i), atts.getLocalName(i), qname));
			}
		}

		super.startElement(uri, localName, qName, atts);
	}
}